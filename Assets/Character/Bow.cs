﻿using UnityEngine;
using System.Collections;

public class Bow : Weapon
{
    private string name;
    private int damage;

    public Bow(string name, int damage)
    {
        this.name = name;
        this.damage = damage;
    }

    public override int getDamage()
    {
        return damage;
    }

    public override string getName()
    {
        return name;
    }


}
