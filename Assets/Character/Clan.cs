﻿using UnityEngine;
using System.Collections;

public class Clan
{
    private string name;

    public Clan(string name)
    {
        this.name = name;
    }

    public string getName()
    {
        return name;
    }
}
