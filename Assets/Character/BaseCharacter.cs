﻿using UnityEngine;
using System.Collections;

public class BaseCharacter
{
    private Sprite sprite;
    private Clan clan;
    private string[] names;
    private int[] strRange, healthRange, manaRange;

    public BaseCharacter(Sprite sprite, Clan clan, int[] strRange, int[] healthRange, int[] manaRange)
    {
        this.manaRange = manaRange;
        this.strRange = strRange;
        this.healthRange = healthRange;
        this.sprite = sprite;

        this.clan = clan;
        names = new string[] { "Ake", "Bard", "Asmund", "Arvid", "Bjarke", "Birger" };
    }

    public Clan getClan()
    {
        return clan;
    }

    public Sprite GetSprite()
    {
        return sprite;
    }

    public Stats GetStats()
    {
        Stats stats = new Stats(strRange, healthRange, manaRange);
        return stats;
    }

    public string GetName()
    {
        return names[Random.Range(0, names.Length)];
    }
}
