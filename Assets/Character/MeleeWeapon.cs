﻿using UnityEngine;
using System.Collections;

public class MeleeWeapon : Weapon
{
    private string name;
    private int[] damageRange;
    private int baseDamage;

    public MeleeWeapon(string name, int[] damageRange, int baseDamage)
    {
        this.name = name;
        this.damageRange = damageRange;
        this.baseDamage = baseDamage;
    }

    public override int getDamage()
    {

        int dmg = Random.Range(damageRange[0], damageRange[1]) + baseDamage;
        return dmg; 
    }

    public override string getName()
    {
        return name;
    }
}
