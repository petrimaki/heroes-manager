﻿using UnityEngine;
using System.Collections;

public class Stats
{
    private int strength;
    private int health;
    private int mana;
    private int maxMana;
    private int maxHealth;

    public int Health { get { return health; } }

    public int Strength { get { return strength; } }

    public int MaxHealth { get { return maxHealth; } }

    public int Mana { get { return mana; } }

    public int MaxMana { get { return maxMana; } }

    public Stats(int[] strRange, int[] healthRange, int[] manaRange)
    {
        this.health = UnityEngine.Random.Range(healthRange[0], healthRange[1]);
        this.maxHealth = this.health;
        this.strength = UnityEngine.Random.Range(strRange[0], strRange[1]);
        this.mana = UnityEngine.Random.Range(manaRange[0], manaRange[1]);
        this.maxMana = mana;
    }

    public void Reset()
    {
        health = maxHealth;
        mana = MaxMana;
    }

    public void ReduceHealth(int dmg)
    {
        this.health = health - dmg;
    }
}
