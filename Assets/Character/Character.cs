﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine;

public class Character
{
    private Sprite sprite;
    private string name;
    private int maxHealth;

    public int Health { get { return stats.Health; } }

    public int MaxHealth { get { return stats.MaxHealth; } }

    public int Mana { get { return stats.Mana; } }

    public int MaxMana { get { return stats.MaxMana; } }

    public int Strength { get { return stats.Strength; } }

    private Bow bow;
    private MeleeWeapon weapon;
    private Clan clan;
    private Stats stats;

    public string ClanName { get { return clan.getName(); } }

    //public Character (string name, Stats stats, Sprite sprite, Bow bow, MeleeWeapon weapon, Clan clan)
    public Character(BaseCharacter baseChar)
    {
        this.name = baseChar.GetName();
        this.stats = baseChar.GetStats();
        this.sprite = baseChar.GetSprite();
        this.weapon = new MeleeWeapon("Fist", new int[] { 0, 2 }, 0);
        this.bow = new Bow("Bow 1", 5);
        this.clan = baseChar.getClan();
    }

    public int getMeleeDamage()
    {
        return weapon.getDamage();
    }

    public int getRangeDamage()
    {
        return bow.getDamage();
    }

    public void setBow(Bow bow)
    {
        this.bow = bow;
    }

    public Bow getBow()
    {
        return bow;
    }

    public void setMeleeWeapon(MeleeWeapon weapon)
    {
        this.weapon = weapon;
    }

    public MeleeWeapon getMeleeWeapon()
    {
        return weapon;
    }

    public void Reset()
    {
        stats.Reset();
    }

    public void ReduceHealth(int dmg)
    {
        stats.ReduceHealth(dmg);
    }

    public Sprite GetSprite()
    {
        return sprite;
    }

    public bool Alive()
    {
        return Health > 0;
    }

    public string GetName()
    {
        return name;
    }
 
}
