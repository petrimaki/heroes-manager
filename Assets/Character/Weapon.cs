﻿using UnityEngine;
using System.Collections;

public abstract class Weapon
{

    public abstract int getDamage();

    public abstract string getName();
}
