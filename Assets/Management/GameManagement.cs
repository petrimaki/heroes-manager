﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManagement : MonoBehaviour
{
    private static GameManagement _instance;
    public bool PlayerWonLastGame = false;

    void Awake()
    {
        if (!_instance)
            _instance = this;
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public Team PlayerTeam;
    public Team Opposite;
    public List<MeleeWeapon> weapons;
    private CharacterPool cPool;

    public CharacterPool getPool()
    {
        return cPool;
    }

    void Start()
    {
        CreateWeapons();
        CreateCharacters();
        CreateTeams();
        CreateLeagues();
        //OnPlayerBattleEnd (true);
        /*AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");

		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("arena.battle.battlearena.MainView");
		//#if UNITY_ANDROID
		currentActivity.Call ("ShowView"); //, unity.GetStatic<AndroidJavaObject> ("currentActivity")
		//#endif*/
        /*
		var androidJC = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		var jo = androidJC.GetStatic<AndroidJavaObject> ("currentActivity");
		// Accessing the class to call a static method on it
		var jc = new AndroidJavaClass ("arena.battle.battle.UnityBridge");
		var menuView = new AndroidJavaClass ("arena.battle.battle.MenuView");
		// Calling a Call method to which the current activity is passed
		jc.CallStatic ("Call", jo, menuView);*/
    }

    void CreateWeapons()
    {
        weapons = new List<MeleeWeapon>();
        weapons.Add(new MeleeWeapon("Fist", new int[] { 0, 2 }, 1));
        weapons.Add(new MeleeWeapon("Bronze dagger", new int[] { 0, 6 }, 1));
    }

    void CreateCharacters()
    {
        cPool = new CharacterPool();
    }

    private List<Team> HeroLeagueTeams;

    public League HeroLeague;
    public List<League> Leagues;

    private void CreateLeagues()
    {
        Leagues = new List<League>();
        HeroLeague = new League("Hero League", HeroLeagueTeams, this);
        Leagues.Add(HeroLeague);
    }

    private void CreateTeams()
    {
        HeroLeagueTeams = new List<Team>();

        for (int x = 0; x < 15; x++)
        {
            HeroLeagueTeams.Add(CreateNewTeam("Team " + x, x + 1));
        }
        PlayerTeam = new PlayerTeam("Player Team", 0);
        PlayerTeam.AddCharacter(cPool.GetRandomNewCharacter());
        PlayerTeam.AddCharacter(cPool.GetRandomNewCharacter());
        Opposite = HeroLeagueTeams[HeroLeagueTeams.Count - 1];
        HeroLeagueTeams.Add(PlayerTeam);

    }

    public void OnPlayerBattleEnd(bool playerWon)
    {
        this.PlayerWonLastGame = playerWon;
        Debug.Log("Player won: " + playerWon);
        HeroLeague.ResolveMatches();
    }

    Team CreateNewTeam(string name, int id)
    {
        AiTeam AiTeam = new AiTeam(name, id);
        var charCount = Random.Range(1, 4);

        for (var x = 0; x < charCount; x++)
        {
            AiTeam.AddCharacter(cPool.GetRandomNewCharacter());
        }
        return AiTeam;
    }
}
