﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterPool
{
    private List<BaseCharacter> BaseCharacters;

    public CharacterPool()
    {
        Create();
    }

    List<Clan> Clans;

    void Create()
    {
        Clans = new List<Clan>();
        BaseCharacters = new List<BaseCharacter>();


        int[] charNumbs = { 1, 4, 7, 10, 42, 52, 55, 58 };
        Sprite[] sprites = Resources.LoadAll<Sprite>("chara4");
        Sprite[] PlayerSprites = Resources.LoadAll<Sprite>("darkknight");

        Clans.Add(new Clan("Thieves"));
        BaseCharacters.Add(
            new BaseCharacter(sprites[1], Clans[0], new int[] { 4, 7 }, new int[] { 10, 15 }, new int[] { 4, 7 })

        );

        /*Clans.Add (new Clan ("Thieves"));
		//Stats stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [1], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [0]
			)
		);

		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [4], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [7], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				PlayerSprites [0], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [10], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [49], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [52], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [55], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);
		Clans.Add (new Clan ("Monks"));
		//stats = new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 });
		Characters.Add (
			new Character (GetName (), new Stats (new int[]{ 4, 7 }, new int[]{ 10, 15 }, new int[]{ 4, 7 }), 
				sprites [58], new Bow ("Bow 1", 5), 
				new MeleeWeapon ("Fist", new int[] { 0, 2 }, 1),
				Clans [1]
			)
		);*/
    }

    public Character GetRandomNewCharacter()
    {
        Character c = new Character(BaseCharacters[Random.Range(0, BaseCharacters.Count)]);
        return c;
    }
}
