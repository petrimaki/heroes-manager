﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class Team
{
    protected List<Character> Characters = new List<Character>();
    protected string name;
    protected int teamId;

    public int getId()
    {
        return teamId;
    }

    public void AddCharacter(Character c)
    {
        Characters.Add(c);
    }


    public List<Character> GetCharacters()
    {
        return Characters.ToList();
    }

    public string getName()
    {
        return name;
    }

    // TODO: Make logic on who AI decides to take on  match squad
    public List<Character> MatchSquad()
    {
        return GetCharacters();
    }

    public float TeamPower()
    {
        float power = 0;
        foreach (Character c in MatchSquad())
        {

        }
        return power;
    }
}
