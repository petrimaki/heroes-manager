﻿using UnityEngine;
using System.Collections;

public class PlayerTeam : Team
{
    private int cash;

    public int Cash { get { return cash; } }

    public PlayerTeam(string name, int id)
    {
        this.name = name;
        this.teamId = id;
    }
}
