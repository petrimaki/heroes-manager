﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class UIManagementMain : MonoBehaviour
{
    Button BattleButton, LadderBtn, TeamBtn, MessageBtn, FightBtn;
    GameObject mainView, messageView, teamView, ladderView;
    private Sprite[] mainButtonStates;
    private GameManagement gm;

    private UIHandleLadders uiLadders;
    private UIHandleTeam uiTeam;

    void Awake()
    {
        LoadResources();

    }

    void Start()
    {
        gm = FindObjectOfType<GameManagement>();
        uiLadders = new UIHandleLadders(gm);
        uiTeam = new UIHandleTeam(GetComponent<MonoBehaviour>());
        FindMenuButtons();
        FindMenuViews();
        BindListeners();
        InitializeView();
    }

    void InitializeView()
    {
        onButtonsChange(false, false, true, false);
    }

    void LoadResources()
    {

        mainButtonStates = new Sprite[2];
        mainButtonStates[0] = Resources.Load("Buttons/Slot2", typeof(Sprite)) as Sprite;
        mainButtonStates[1] = Resources.Load("Buttons/Slot2_selected", typeof(Sprite)) as Sprite;
    }

    void FindMenuButtons()
    {
        BattleButton = GameObject.Find("BattleButton").GetComponent<Button>();
        LadderBtn = GameObject.Find("Ladder").GetComponent<Button>();
        TeamBtn = GameObject.Find("Team").GetComponent<Button>();
        MessageBtn = GameObject.Find("Messages").GetComponent<Button>();
        FightBtn = GameObject.Find("Fight").GetComponent<Button>();

    }

    void FindMenuViews()
    {

        ladderView = GameObject.Find("LadderView");
        ladderView.SetActive(false);
        teamView = GameObject.Find("TeamView");
        teamView.SetActive(false);
        messageView = GameObject.Find("MessageView");
        messageView.SetActive(false);
        mainView = GameObject.Find("MainView");
    }

    void BindListeners()
    {
        BattleButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(1);
        });
        LadderBtn.onClick.AddListener(() =>
        {
            onButtonsChange(true, false, false, false);
        });
        MessageBtn.onClick.AddListener(() =>
        {
            onButtonsChange(false, false, false, true);
        });
        TeamBtn.onClick.AddListener(() =>
        {
            onButtonsChange(false, true, false, false);
            uiTeam.becomeActive();
        });
        FightBtn.onClick.AddListener(() =>
        {
            onButtonsChange(false, false, true, false);
        });
    }

    private void onButtonsChange(bool first, bool second, bool third, bool forth)
    {
        string normal = "FFFFFFFF";
        LadderBtn.GetComponent<Image>().sprite = first == false ? mainButtonStates[0] : mainButtonStates[1];
        TeamBtn.GetComponent<Image>().sprite = second == false ? mainButtonStates[0] : mainButtonStates[1];
        FightBtn.GetComponent<Image>().sprite = third == false ? mainButtonStates[0] : mainButtonStates[1];
        MessageBtn.GetComponent<Image>().sprite = forth == false ? mainButtonStates[0] : mainButtonStates[1];
        ladderView.SetActive(first);
        teamView.SetActive(second);
        messageView.SetActive(forth);
        mainView.SetActive(third);

        handleOtherWindowClosing();
    }

    // User change bottom window view
    public void handleOtherWindowClosing()
    {
        uiLadders.OnWindowChange();
    }

    public static Color hexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }
}
