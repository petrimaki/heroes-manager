﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIHandleTeam
{
    GameObject TeamMembers, TeamButtons, ShopView, TeamView, UserHolder, RecruitGladiators, divisionContainer, GladiatorHolder, BuyGladiator;
    Button MyGladiators, Shop, Recruit, CloseShop, CloseTeam, CloseRecruit;
    MonoBehaviour mono;

    public UIHandleTeam(MonoBehaviour mono)
    {
        LoadResoources();
        FindButtons();
        BindListeners();
        this.mono = mono;
        FindViews();
    }

    void LoadResoources()
    {
        GladiatorHolder = (GameObject)Resources.Load("MainUI/GladiatorHolder");
    }

    public void becomeActive()
    {
        TeamMembers.SetActive(false);
        TeamButtons.SetActive(true);
        TeamView.SetActive(true);
        UserHolder.SetActive(true);
    }

    void FindViews()
    {
        divisionContainer = GameObject.Find("RecruitGladiatorContainer");
        BuyGladiator = GameObject.Find("BuyGladiator");
        BuyGladiator.SetActive(false);
        TeamView = GameObject.Find("TeamView");
        TeamButtons = GameObject.Find("TeamButtons");
        TeamMembers = GameObject.Find("TeamMembers");
        TeamMembers.SetActive(false);
        ShopView = GameObject.Find("ShopView");
        UserHolder = GameObject.Find("UserHolder");
        ShopView.SetActive(false);

        CloseTeam.gameObject.SetActive(false);
        RecruitGladiators = GameObject.Find("RecruitGladiators");
        RecruitGladiators.SetActive(false);
    }

    void FindButtons()
    {
        MyGladiators = GameObject.Find("MyGladiators").GetComponent<Button>();
        Shop = GameObject.Find("Shop").GetComponent<Button>();
        Recruit = GameObject.Find("Recruit").GetComponent<Button>();
        CloseShop = GameObject.Find("CloseShop").GetComponent<Button>();
        CloseTeam = GameObject.Find("CloseTeam").GetComponent<Button>();
        CloseRecruit = GameObject.Find("CloseRecruit").GetComponent<Button>();
    }

    void BindListeners()
    {

        CloseTeam.onClick.AddListener(() =>
        {
            CloseTeam.gameObject.SetActive(false);
            TeamMembers.SetActive(false);
            TeamButtons.SetActive(true);
        });
        CloseShop.onClick.AddListener(() =>
        {
            ShopView.SetActive(false);
            becomeActive();
        });

        MyGladiators.onClick.AddListener(() =>
        {
            CloseTeam.gameObject.SetActive(true);
            TeamMembers.SetActive(true);
            TeamButtons.SetActive(false);
        });

        Shop.onClick.AddListener(() =>
        {
            TeamView.SetActive(false);
            UserHolder.SetActive(false);
            TeamButtons.SetActive(false);
            ShopView.SetActive(true);
            mono.StartCoroutine(LateUpdateCanvas());
        });

        Recruit.onClick.AddListener(() =>
        {
            TeamView.SetActive(false);
            UserHolder.SetActive(false);
            TeamButtons.SetActive(false);
            RecruitGladiators.SetActive(true);
            DrawCurrentRecruits();
            //mono.StartCoroutine (LateUpdateCanvas ());
        });

        CloseRecruit.onClick.AddListener(() =>
        {
            RecruitGladiators.SetActive(false);
            becomeActive();
        });
    }

    void DrawCurrentRecruits()
    {
        var children = new List<GameObject>();
        foreach (Transform child in divisionContainer.transform)
        {
            children.Add(child.gameObject);
        }
        children.ForEach(child => GameObject.Destroy(child));
        //List<Rank> divTeams = d.GetDivisionScores ();
        for (var x = 0; x < 4; x++)
        {
            GameObject el = null;
            GladiatorHolder.GetComponent<Button>().onClick.AddListener(() =>
            {
                Debug.Log("Wanna by place " + x);
            });
            //DivisionTeam.transform.FindChild ("TeamScore").GetComponent<Text> ().text = divTeams [x].score.ToString ();
            el = GameObject.Instantiate(GladiatorHolder);
            var xx = x;
            el.GetComponent<Button>().onClick.AddListener(() =>
            {
                Debug.Log("Wanna by place " + xx);
                ShowBuyGladiatorView();
            });
            el.transform.SetParent(divisionContainer.transform);
            el = null;
            //xx = null;
        }
    }

    void ShowBuyGladiatorView()
    {
        BuyGladiator.SetActive(true);
        BuyGladiator.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
            //CharacterPool pool = UnityEngine.GameObject.FindObjectOfType<>
            GameManagement management = UnityEngine.GameObject.FindObjectOfType<GameManagement>();//.PlayerTeam.AddCharacter()
            management.PlayerTeam.AddCharacter(management.getPool().GetRandomNewCharacter());
            BuyGladiator.SetActive(false);
        });
        //RecruitButton.get
    }

    // TODO Make better? This Updates shop items in canvas scroll container
    public IEnumerator LateUpdateCanvas()
    {
        yield return new WaitForSeconds(0.05f);
        Canvas.ForceUpdateCanvases();
        RectTransform currentShopRect = GameObject.Find("ShopItemsContent").GetComponent<RectTransform>();

        // 2640 = Numbero of items * items height
        currentShopRect.sizeDelta = new Vector2(currentShopRect.sizeDelta.x, 2640);

        Canvas.ForceUpdateCanvases();
        yield return new WaitForSeconds(0.05f);
        Debug.Log("CanvasUp:");
        // Scroll to start
        GameObject.Find("ShopScrollBar").GetComponent<Scrollbar>().value = 1f;
        GameObject.Find("ShopScrollBar").GetComponent<Scrollbar>().size = 1f;
        Canvas.ForceUpdateCanvases();
    }
}
