﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIHandleLadders
{
    Button Div1, Div2, Div3, Div4, LeagueBackButton;
    GameObject DivisionTeam, divisionContainer, ViewDivision, LeagueBack;
    private GameManagement gm;


    public UIHandleLadders(GameManagement gm)
    {
        this.gm = gm;
        LoadResoources();
        FindElemets();
        BindListeners();
        FindViews();
    }

    void LoadResoources()
    {
        DivisionTeam = (GameObject)Resources.Load("MainUI/DivisionElement");
    }

    void FindViews()
    {
        divisionContainer = GameObject.Find("DivisionContainer");
        ViewDivision = GameObject.Find("ViewDivision");
        ViewDivision.SetActive(false);
    }

    void FindElemets()
    {

        // Division buttons
        Div1 = GameObject.Find("Div1").GetComponent<Button>();
        Div2 = GameObject.Find("Div2").GetComponent<Button>();
        Div3 = GameObject.Find("Div3").GetComponent<Button>();
        Div4 = GameObject.Find("Div4").GetComponent<Button>();
        LeagueBack = GameObject.Find("LeagueBack");
        LeagueBackButton = LeagueBack.GetComponent<Button>();
        LeagueBack.SetActive(false);
    }

    void BindListeners()
    {
        Div4.onClick.RemoveAllListeners();
        Div1.onClick.RemoveAllListeners();
        Div2.onClick.RemoveAllListeners();
        Div3.onClick.RemoveAllListeners();

        DrawAllLeagues();
    }

    void DrawSingleLeague(League l)
    {
        ViewDivision.SetActive(false);
        LeagueBack.SetActive(true);
        LeagueBackButton.onClick.RemoveAllListeners();
        Div1.onClick.RemoveAllListeners();
        Div2.onClick.RemoveAllListeners();
        Div3.onClick.RemoveAllListeners();
        Div4.onClick.RemoveAllListeners();

        Div1.interactable = true;
        Div2.interactable = true;
        Div3.interactable = true;
        Div4.interactable = true;

        LeagueBackButton.onClick.AddListener(() =>
        {
            DrawAllLeagues();
        });

        Div1.gameObject.GetComponentInChildren<Text>().text = l.GetDivisions()[0].Name;
        Div1.onClick.AddListener(() =>
        {
            DrawDivision(l.GetDivisions()[0]);
        });
        Div2.gameObject.GetComponentInChildren<Text>().text = l.GetDivisions()[1].Name;
        Div2.onClick.AddListener(() =>
        {
            DrawDivision(l.GetDivisions()[1]);
        });
        Div3.gameObject.GetComponentInChildren<Text>().text = l.GetDivisions()[2].Name;
        Div3.onClick.AddListener(() =>
        {
            DrawDivision(l.GetDivisions()[2]);
        });
        Div4.gameObject.GetComponentInChildren<Text>().text = l.GetDivisions()[3].Name;
        Div4.onClick.AddListener(() =>
        {
            DrawDivision(l.GetDivisions()[3]);
        });
    }

    void DrawAllLeagues()
    {
        List<League> Leagues = gm.Leagues;
        LeagueBack.SetActive(false);
        LeagueBackButton.onClick.RemoveAllListeners();
        Div1.onClick.RemoveAllListeners();
        Div2.onClick.RemoveAllListeners();
        Div3.onClick.RemoveAllListeners();
        Div4.onClick.RemoveAllListeners();

        Div1.interactable = true;
        Div2.interactable = true;
        Div3.interactable = true;
        Div4.interactable = true;

        Div1.gameObject.GetComponentInChildren<Text>().text = Leagues[0].Name;
        Div1.onClick.AddListener(() =>
        {
            DrawSingleLeague(Leagues[0]);
        });
        Div2.gameObject.GetComponentInChildren<Text>().text = Leagues[0].Name;
        Div2.onClick.AddListener(() =>
        {
            DrawSingleLeague(Leagues[0]);
        });
        Div3.gameObject.GetComponentInChildren<Text>().text = Leagues[0].Name;
        Div3.onClick.AddListener(() =>
        {
            DrawSingleLeague(Leagues[0]);
        });
        Div4.gameObject.GetComponentInChildren<Text>().text = Leagues[0].Name;
        Div4.onClick.AddListener(() =>
        {
            DrawSingleLeague(Leagues[0]);
        });
    }

    void DrawDivision(Division d)
    {
        ViewDivision.SetActive(true);
        LeagueBack.SetActive(true);
        LeagueBackButton.onClick.RemoveAllListeners();

        LeagueBackButton.onClick.AddListener(() =>
        {
            DrawSingleLeague(d.League);
        });
        var children = new List<GameObject>();
        foreach (Transform child in divisionContainer.transform)
        {
            children.Add(child.gameObject);
        }
        children.ForEach(child => GameObject.Destroy(child));
        List<Rank> divTeams = d.GetDivisionScores();
        for (var x = 0; x < 4; x++)
        {
            DivisionTeam.transform.Find("TeamName").GetComponent<Text>().text = divTeams[x].team.getName();
            DivisionTeam.transform.Find("TeamScore").GetComponent<Text>().text = divTeams[x].score.ToString();
            GameObject el = GameObject.Instantiate(DivisionTeam);
            el.transform.SetParent(divisionContainer.transform);
        }
    }

    void OnBackButtonPressed()
    {

    }

    public void OnWindowChange()
    {
        ViewDivision.SetActive(false);
    }

}
