﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DivisionSchedule
{
    private List<Team> teams;
    private List<ScheduledMatch> matches;
    private GameManagement management;

    public DivisionSchedule(List<Team> teams, GameManagement management)
    {
        this.teams = teams;
        this.management = management;
        CreateMatches();
    }

    private void CreateMatches()
    {
        matches = new List<ScheduledMatch>();
        CreateFirstRound();
        CreateSecondRound();
        for (var x = 0; x < 8; x++)
        {
            string roundText = "";
            foreach (ScheduledMatch m in matches)
            {
                if (m.getRound() == x)
                {
                    roundText += " | " + m.GetVsText();
                }
            }
            Debug.Log("Division round " + x + " : " + roundText);
        }
    }

    void CreateFirstRound()
    {
        int round = 0;
        for (var x = 1; x < 4; x++)
        {
            matches.Add(new ScheduledMatch(teams[0], teams[x], round));
            round++;
        }
        round = 2;
        for (var x = 2; x < 4; x++)
        {
            matches.Add(new ScheduledMatch(teams[1], teams[x], round));
            round--;
        }
        matches.Add(new ScheduledMatch(teams[2], teams[3], 0));
    }

    void CreateSecondRound()
    {
        int round = 3;
        for (var x = 1; x < 4; x++)
        {
            matches.Add(new ScheduledMatch(teams[0], teams[x], round));
            round++;
        }
        round = 5;
        for (var x = 2; x < 4; x++)
        {
            matches.Add(new ScheduledMatch(teams[1], teams[x], round));
            round--;
        }
        matches.Add(new ScheduledMatch(teams[2], teams[3], 3));
    }

    public List<Team> ActRound(int round)
    {
        List<Team> winners = new List<Team>();
        foreach (ScheduledMatch m in matches)
        {
            if (m.getRound() == round)
            {
                if (m.isNotPlayerMatch())
                {
                    winners.Add(m.Winner());
                }
                else
                {
                    Team t = management.PlayerWonLastGame == false ? management.Opposite : management.PlayerTeam;
                    winners.Add(t);
                }
            }
        }

        return winners;
    }

    public ScheduledMatch FindTeamCurrentMatch(int teamID)
    {
        foreach (ScheduledMatch m in matches)
        {
            if (m.HasTeamWithID(teamID) != null)
            {
                return m;
            }

        }
        return null;
    }
}
