﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class League
{
    private List<Team> LeagueTeams;
    private List<Division> divisions;
    private string leagueName;
    private int round;
    private GameManagement management;

    public string Name { get { return leagueName; } }

    public League(string leagueName, List<Team> teams, GameManagement management)
    {
        this.round = 0;
        this.leagueName = leagueName;
        divisions = new List<Division>();
        LeagueTeams = teams;
        this.management = management; 
        for (var x = 0; x < 4; x++)
        {
            var i = x * 4;
            divisions.Add(new Division("Division " + (x + 1), teams.GetRange(i, 4), this));
        }
    }

    public GameManagement getManagement()
    {
        return management;
    }

    public List<Division> GetDivisions()
    {
        return divisions;
    }

    public void ResolveMatches()
    {
        foreach (Division d in divisions)
        {
            d.ActDivisionRound(round);
        }
        round++;
    }

    public Division GetDivisionById(int id)
    {
        return divisions[id];
    }

    public ScheduledMatch GetTeamMatch(int teamID)
    {
        foreach (Division d in divisions)
        {
            return d.FindTeamMatch(teamID);
        }
        return null;
    }


}
