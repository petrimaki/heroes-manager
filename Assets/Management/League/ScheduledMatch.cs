﻿using UnityEngine;
using System.Collections;

public class ScheduledMatch
{
    private Team team1;
    private Team team2;
    private int round;

    public ScheduledMatch(Team team1, Team team2, int round)
    {
        this.team1 = team1;
        this.team2 = team2;
        this.round = round;
    }

    public int getRound()
    {
        return round;
    }

    public string GetVsText()
    {
        return team1.getName() + " vs. " + team2.getName();
    }

    public bool isNotPlayerMatch()
    {
        return (team1.GetType() != typeof(PlayerTeam) && team2.GetType() != typeof(PlayerTeam));
    }

    public Team Winner()
    {
        return team1;
    }

    public ScheduledMatch HasTeamWithID(int teamID)
    {
        if (team1.getId() == teamID || team2.getId() == teamID)
        {
            return this;
        }
        return null;
    }
}
