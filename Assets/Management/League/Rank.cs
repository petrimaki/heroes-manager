﻿using UnityEngine;
using System.Collections;

public class Rank
{
    public Team team;
    public int score;

    public Rank(Team team, int score)
    {
        this.team = team;
        this.score = score;
    }

    public void increaseScore(int score)
    {
        this.score += score;
    }
}