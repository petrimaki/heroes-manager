﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Division
{
    private List<Team> teams;
    private string name;
    private GameManagement management;

    public string Name { get { return name; } }

    private DivisionSchedule scheduled;
    private League myLeague;

    public League League { get { return myLeague; } }

    private List<Rank> DivisionRanking;

    public Division(string name, List<Team> teams, League league)
    {
        this.teams = teams;
        this.name = name;
        this.myLeague = league;
        this.management = league.getManagement();
        scheduled = new DivisionSchedule(teams, management);
        CreateRankings();
    }

    void CreateRankings()
    {
        DivisionRanking = new List<Rank>();
        for (var x = 0; x < 4; x++)
        {
            DivisionRanking.Add(new Rank(teams[x], 0));
        }
    }

    public void ActDivisionRound(int round)
    {
        List<Team> winners = scheduled.ActRound(round);
        Debug.Log("winners: " + winners.Count);
        foreach (Team t in winners)
        {
            Rank r = DivisionRanking.Find((Rank obj) => obj.team.getId() == t.getId());
            if (r != null)
            {
                r.increaseScore(3);
            }
            else
            {
                Debug.LogError("No winner found from division");
            }

        }
        foreach (Rank r in DivisionRanking)
        {
            Debug.Log("Ranking: " + r.team.getName() + " score: " + r.score);
        }
    }

    public List<Rank> GetDivisionScores()
    {
        List<Rank> ranks = DivisionRanking.ToList();
        ranks = ranks.OrderByDescending((Rank arg) => arg.score).ToList();
        return ranks;
    }

    public ScheduledMatch FindTeamMatch(int teamID)
    {
        return scheduled.FindTeamCurrentMatch(teamID);
    }
}
