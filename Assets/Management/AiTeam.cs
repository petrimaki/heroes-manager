﻿using UnityEngine;
using System.Collections;

public class AiTeam : Team
{

    public AiTeam(string name, int id)
    {
        this.name = name;
        this.teamId = id;
    }
}
