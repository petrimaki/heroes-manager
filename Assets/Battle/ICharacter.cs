﻿using UnityEngine;
using System.Collections;
using System;

public interface ICharacter
{
	void setCurrentPlatform (MoveHandler plat) ;

	MoveHandler getCurrentPlatform () ;

	void SetMoved (bool status);

	bool IsCurrentCharacter ();

	Vector3 getPosition ();

	void SetPosition (Vector3 position);

	void MakeAttackHit (ICharacter character, Action<string> callback);

	void MakeAttackShoot (ICharacter character, Action<string> callback);

	void onAttacked (int dmg, Action<bool> callback);

	Character GetCharacter ();

	bool isAlive ();
}
