﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveHandler : MonoBehaviour
{
    public bool IsOccupied = false;
    public GameObject character;
    public int row, cell;
    private Renderer render;
    private GameObject moveHandler;
    private GameObject activeImage;

    void Awake()
    {
        render = GetComponent<Renderer>();
        character = null;
        GameObject canvas = transform.Find("Canvas").gameObject;
        this.moveHandler = canvas.transform.Find("MoveHandler").gameObject;
        moveHandler.GetComponent<Button>().onClick.AddListener(() =>
        {
            onMovementSelected();
        });
        this.moveHandler.SetActive(false);
        this.activeImage = canvas.transform.Find("ActiveImage").gameObject;
        activeImage.SetActive(false);
    }

    public bool IsOccupiedAI()
    {
        bool occ = (character != null && character.GetComponent<Ai>() != null);
        if (character != null)
        {
            Debug.Log("OCC: " + occ + " AI: " + (character.GetComponent<Ai>() != null) + " char: " + (character != null));

        }
        return occ;
    }

    public void setIDS(int row, int cell)
    {
        this.row = row;
        this.cell = cell;

    }

    public void OnDeathHere()
    {
        IsOccupied = false;
        character = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        IsOccupied = true;
        character = other.gameObject;
        Debug.Log("Enter");
    }

    void OnTriggerExit2D(Collider2D other)
    {
        IsOccupied = false;
        character = null;
    }

    public void SetIsActivePlatform(bool active)
    {
        activeImage.SetActive(active);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log ("OnTriggerStay2D");
        IsOccupied = true;
        character = other.gameObject;
    }

    public bool EqualsMovementPlatform(int r, int c)
    {
        return (r == row && c == cell);
    }

    private void onMovementSelected()
    {
        activeChracter.MakeMove(this);
    }

    private Controllers activeChracter;

    public void DisplayMovement(bool status, Controllers c)
    {
        this.activeChracter = c;
        if (character != null && IsOccupied)
        {
            Debug.Log("Here");
            Ai ai = character.GetComponent<Ai>();
            if (ai)
            {
                ai.onDisplayAttackSword(status);
            }
            else
            {
                if (!IsOccupied)
                {
                    moveHandler.SetActive(status);
                }
            }
        }
        else
        {
            if (!IsOccupied)
            {
                moveHandler.SetActive(status);
            }
        }
    }
}
