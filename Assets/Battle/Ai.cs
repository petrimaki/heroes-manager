﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class Ai : Actor, ICharacter
{
    GameObject player;
    // Use this for initialization
    private Character myCharacter;

    private bool moved;


    protected override void OnStart()
    {
        this.moved = false;

    }

    public void SetupCharacter(Character c, MoveHandler moveHandler)
    {
        this.myCharacter = c;
        this.currentPlatform = moveHandler;
    }

    protected override void OnUpdate()
    {

    }

    public void Act()
    {
        CurrentCharacter = this.gameObject.GetComponent<Ai>();
        currentPlatform.SetIsActivePlatform(true);
        StartCoroutine(StartActing());
    }

    private IEnumerator StartActing()
    {
        yield return new WaitForSeconds(1f);
        if (!isAlive())
        {
            Debug.LogError("AI is acting while death!");
        }
        foreach (Player p in FindObjectsOfType<Player>())
        {
            if (p.isAlive())
            {
                player = p.gameObject;
                break;
            }
        }
        DecideAction(() =>
        {
            currentPlatform.SetIsActivePlatform(false);
            SetMoved(true);
            StartCoroutine(EndTurnAfter());
        });

    }

    IEnumerator EndTurnAfter()
    {
        yield return new WaitForSeconds(0.3f);
        FindObjectOfType<World>().EndTurn(false);
    }

    public bool isAlive()
    {
        return myCharacter.Alive();
    }

    private IEnumerator TryShoot(Action calllback)
    {
        OnStartBowShoot.SetActive(true);
        yield return new WaitForSeconds(1.25f);
        foreach (Player pla in FindObjectsOfType<Player>())
        {
            if (!pla.isAlive())
            {
                continue;
            }
            else
            {
                MakeAttackShoot(pla, (string dmg) =>
                {
                    Debug.Log("I am shooting player, made " + dmg + " dmg");
                    OnStartBowShoot.SetActive(false);
                    calllback();
                    return;
                });
                break;
            }

        }
    }

    private void DecideAction(Action calllback)
    {
        Debug.Log("Ai is deciding action");
        TryMove(calllback);
    }

    private void TryMove(Action calllback)
    {
        int currentRow = currentPlatform.row;
        int currentCell = currentPlatform.cell;
        MoveHandler movePoint;
        List<MoveHandler> moveOptions = getMovementPoints();
        GameObject closestPoint = null;
        List<Player> InAttackDistance = new List<Player>();
        closestPoint = currentPlatform.gameObject;
        foreach (MoveHandler m in moveOptions)
        {
            GameObject point = m.gameObject;
            movePoint = point.GetComponent<MoveHandler>();
            if (DistanceToPlayer(player.transform.position, point.transform.position, closestPoint.transform.position))
            {
                if (movePoint.IsOccupied)
                {
                    if (movePoint.character != null)
                    {
                        Player playerClass = movePoint.character.GetComponent<Player>();
                        if (playerClass != null && playerClass.isAlive())
                        {
                            InAttackDistance.Add(playerClass);
                        }
                    }
                }
                else
                {
                    closestPoint = point;
                }
            }
            else if (movePoint.IsOccupied)
            {
                if (movePoint.character != null)
                {
                    Player playerClass = movePoint.character.GetComponent<Player>();
                    if (playerClass != null && playerClass.isAlive())
                    {
                        InAttackDistance.Add(playerClass);
                    }
                }
            }
        }
        if (InAttackDistance.Count > 0)
        {
            MakeAttackHit(InAttackDistance[0], (string dmg) =>
            {
                Debug.Log("I Made " + dmg + " dmg to " + InAttackDistance[0].GetCharacter().GetName());
                calllback();
            });
        }
        else
        {
            if (myCharacter.getBow() != null)
            {
                StartCoroutine(TryShoot(calllback));
            }
            else
            {
                movePoint = closestPoint.GetComponent<MoveHandler>();
                if (!movePoint.IsOccupied)
                {
                    currentPlatform.SetIsActivePlatform(false);
                    transform.position = new Vector3(closestPoint.transform.position.x, closestPoint.transform.position.y, 0);
                    currentPlatform = movePoint;
                }
                calllback();
            }
        }
    }

    private bool DistanceToPlayer(Vector3 player, Vector3 newPosition, Vector3 currentPosition)
    {
        float currentPointDist = Vector3.Distance(player, currentPosition);
        float newPositionDist = Vector3.Distance(player, newPosition);
        if (newPositionDist <= currentPointDist)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void MakeAttackHit(ICharacter character, Action<string> callback)
    {
        Debug.Log("Making Attack!");
        int attackdmg = myCharacter.getMeleeDamage();
        character.onAttacked(attackdmg, (isDeath) =>
        {
            callback(attackdmg.ToString());
        });

    }

    public void MakeAttackShoot(ICharacter character, Action<string> callback)
    {
        Debug.Log("Making Attack!");
        int attackdmg = UnityEngine.Random.Range(0, 4);
        character.onAttacked(attackdmg, (isDeath) =>
        {
            callback(attackdmg.ToString());
        });
    }

    public void onAttacked(int damage, Action<bool> callback)
    {
        StartCoroutine(AnimateDamage(damage));
        myCharacter.ReduceHealth(damage);
        if (myCharacter.Health <= 0)
        {
            Debug.Log("Ai is death!");
            StartCoroutine(OnDeathAnimation(callback));
        }
        else
        {
            callback(false);
        }
        Debug.Log("AI get healt: " + myCharacter.Health);
    }

    public void SetMoved(bool m)
    {
        this.moved = m;
    }

    public bool IsMoved()
    {
        return moved;
    }

    public void onDisplayAttackSword(bool status)
    {
        if (!isAlive())
        {
            return;
        }
        swordsHolder.SetActive(status);
        /*
        	if (status == true) {
                //swordsHolder.GetComponent<Animator> ().Play ("Base.OnAttackSwordsSwing", 0, 1f);
            } 
		*/
    }

    public void onDisplayAttackBow(bool status)
    {
        if (!isAlive())
        {
            return;
        }
        bowHolder.SetActive(status);
        /*if (status == true)
        {
            //swordsHolder.GetComponent<Animator> ().Play ("Base.OnAttackSwordsSwing", 0, 1f);
        }*/
    }

    public bool IsCurrentCharacter()
    {
        return false;
    }

    public void setCurrentPlatform(MoveHandler moveHandler)
    {
        this.currentPlatform = moveHandler;
    }

    public MoveHandler getCurrentPlatform()
    {
        return currentPlatform;
    }

    public Vector3 getPosition()
    {
        return this.gameObject.transform.position;
    }

    public Character GetCharacter()
    {
        Debug.Log("My character:" + myCharacter.GetName() + " health: " + myCharacter.Health);
        return myCharacter;
    }

    public void SetPosition(Vector3 position)
    {
        this.gameObject.transform.position = position;
    }
}
