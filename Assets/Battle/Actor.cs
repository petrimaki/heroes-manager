﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public abstract class Actor : MonoBehaviour
{
    protected ICharacter CurrentCharacter;
    protected MoveHandler[] moveHandlers;
    protected Animator am, deathAm;
    protected int bloodAnimation, deathAnimation;
    protected GameObject OnDamageTakenCanvasElement;
    protected SpriteRenderer rend;
    protected World world;
    private BoxCollider2D coll;
    protected MoveHandler currentPlatform;
    //protected ICharacter ActiveCharacter{ get { return world.GetCurrentCharacter (); } }
    protected GameObject swordsHolder, bowHolder, OnStartBowShoot;

    void Start()
    {
        coll = this.gameObject.GetComponent<BoxCollider2D>();
        world = FindObjectOfType<World>();
        Debug.Log("world: " + world);
        moveHandlers = GameObject.FindObjectsOfType<MoveHandler>();
        rend = GetComponent<SpriteRenderer>();

        FindObjects();
        OnStartBowShoot.SetActive(false);
        swordsHolder.SetActive(false);
        bowHolder.SetActive(false);
        OnStart();
    }

    private void FindObjects()
    {

        OnDamageTakenCanvasElement = this.gameObject.transform.Find("CharacterCanvas").gameObject;//.transform.FindChild ("DamageContainer").gameObject;
        am = OnDamageTakenCanvasElement.transform.Find("DamageContainer").gameObject.GetComponent<Animator>();
        deathAm = OnDamageTakenCanvasElement.transform.Find("DeathImage").gameObject.GetComponent<Animator>();
        OnDamageTakenCanvasElement.transform.Find("DeathImage").gameObject.SetActive(false);
        OnDamageTakenCanvasElement.transform.Find("DamageContainer").gameObject.SetActive(false);
        Debug.Log("am: " + am);
        bloodAnimation = Animator.StringToHash("Base.OnDamageTakenCanvas");
        deathAnimation = Animator.StringToHash("Base.OnDeathAnimation");

        GameObject charCanvas = transform.Find("CharacterCanvas").gameObject;
        swordsHolder = charCanvas.transform.Find("AttackSwords").gameObject;
        bowHolder = charCanvas.transform.Find("AttackArrow").gameObject;
        OnStartBowShoot = charCanvas.transform.Find("OnStartBowShoot").gameObject;

    }

    protected abstract void OnStart();

    void Update()
    {
        OnUpdate();
    }

    protected abstract void OnUpdate();

    public bool CanMoveToPlatform(MoveHandler plat)
    {
        List<MoveHandler> possibleMovenets = getMovementPoints();
        foreach (MoveHandler c in possibleMovenets)
        {
            if (plat.EqualsMovementPlatform(c.row, c.cell) && !plat.IsOccupied)
            {
                return true;
            }
        }
        return false;
    }

    public List<MoveHandler> getMovementPoints()
    {
        List<MoveHandler> possibleMovenets = new List<MoveHandler>();
        int currentRow = world.GetCurrentCharacter().getCurrentPlatform().row;
        int currentCell = world.GetCurrentCharacter().getCurrentPlatform().cell;
        for (int x = 0; x < moveHandlers.Length; x++)
        {
            MoveHandler plat = moveHandlers[x];
            if (isInMoveDistance(plat, currentRow, currentCell) && !plat.EqualsMovementPlatform(currentRow, currentCell))
            {
                possibleMovenets.Add(plat);
            }
        }
        return possibleMovenets;
    }

    public bool isInMoveDistance(MoveHandler plat, int row, int cell)
    {
        int rowBelow = plat.row - 1;
        int topRow = plat.row + 1;
        if (row == topRow)
        {
            if (CellEquals(plat, cell))
            {
                return true;
            }
        }
        else if (row == plat.row)
        {
            if (CellEquals(plat, cell))
            {
                return true;
            }
        }
        else if (rowBelow == row)
        {
            if (CellEquals(plat, cell))
            {
                return true;
            }
        }
        return false;
    }

    protected bool CellEquals(MoveHandler plat, int cell)
    {
        return (plat.cell + 1 == cell || cell == plat.cell || plat.cell - 1 == cell);
    }

    protected IEnumerator AnimateDamage(int dmg)
    {
        GameObject damageContainer = OnDamageTakenCanvasElement.transform.Find("DamageContainer").gameObject;
        damageContainer.SetActive(true);
        damageContainer.GetComponentInChildren<Text>().text = dmg.ToString();

        am.Play("OnDamageTakenCanvas", -1, 0f);
        yield return new WaitForSeconds(2f);
        damageContainer.SetActive(false);
    }

    protected IEnumerator OnDeathAnimation(Action<bool> callback)
    {
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        currentPlatform.OnDeathHere();
        coll.enabled = false;
        deathAm.gameObject.SetActive(true);
        deathAm.Play("OnDeathAnimation", -1, 0f);
        yield return new WaitForSeconds(2f);
        deathAm.gameObject.SetActive(false);
        callback(true);
    }
}
