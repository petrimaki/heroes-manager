﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controllers : MonoBehaviour
{
    RuntimePlatform platform = Application.platform;
    private BattleCanvas battleCanvas;
    private World world;
    private Player CurrentCharacter;
    protected MoveHandler[] moveHandlers;

    void Start()
    {
        battleCanvas = FindObjectOfType<BattleCanvas>();
        world = FindObjectOfType<World>();
        moveHandlers = GameObject.FindObjectsOfType<MoveHandler>();
    }

    private Vector2 touchCache;
    private bool dragging = false;
    private Vector3 v3, offset;
    private Transform toDrag;
    private float dist;

    void Update()
    {
        if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount > 0)
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    checkTouch(Input.GetTouch(0).position);
                }
            }
        }
        else if (platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                checkTouch(Input.mousePosition);
            }
        }
    }

    public void OnTurnChange(Player p)
    {
        this.CurrentCharacter = p;
    }

    void checkTouch(Vector3 pos)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
        var layerMask = ~(1 << 8);
        var hit = Physics2D.OverlapPoint(new Vector2(wp.x, wp.y), layerMask);
        if (hit)
        {
            Debug.Log("hit: " + hit.gameObject.name);
            Player character = hit.GetComponent<Player>();
            MoveHandler movement = hit.GetComponent<MoveHandler>();
            Ai ai = hit.GetComponent<Ai>();

            if (!battleCanvas.UsingAbilities())
            {
                if (character != null && character.IsCurrentCharacter())
                {
                    if (isDisplayingActions)
                    {
                        // Display current character stats
                        FindObjectOfType<BattleCanvas>().SelectedCharacter(character.GetCharacter());
                    }
                    else
                    {
                        DisplayPossibleActions(true);
                    }
                    Debug.Log("Display action");

                }
                else if (character != null && ai == null)
                {
                    // Display teammate stats
                    FindObjectOfType<BattleCanvas>().SelectedCharacter(character.GetCharacter());
                }
                else if (ai == null)
                {
                    Debug.Log("Dont display action");
                    DisplayPossibleActions(false);
                }
            }
            if (ai != null)
            {
                Debug.Log("Ai else than null");
                // Hit close
                if (CurrentCharacter != null && isDisplayingActions)
                {

                    int row = CurrentCharacter.getCurrentPlatform().row;
                    int cell = CurrentCharacter.getCurrentPlatform().cell;
                    if (CurrentCharacter.isInMoveDistance(ai.getCurrentPlatform(), row, cell))
                    {
                        DisplayPossibleActions(false);
                        StartCoroutine(CurrentCharacter.HitCoroutine(ai));
                    }
                    else
                    {
                        FindObjectOfType<BattleCanvas>().SelectedCharacter(ai.GetCharacter());
                    }
                }
                // Shoot / Magic
                else if (battleCanvas.UsingAbilities())
                {
                    // Close shooting icons
                    Ai[] AIs = FindObjectsOfType<Ai>();
                    foreach (Ai a in AIs)
                    {
                        a.onDisplayAttackBow(false);
                    }
                    StartCoroutine(CurrentCharacter.ShotCoroutine(ai));
                }
                else
                {
                    FindObjectOfType<BattleCanvas>().SelectedCharacter(ai.GetCharacter());
                }
                if (battleCanvas.UsingAbilities())
                {
                    battleCanvas.resetCanvasSelections();
                } 
            }
        }
        else
        {
            Debug.Log("CloseActions");
            StartCoroutine(CloseActions());
        }
    }

    IEnumerator CloseActions()
    {
        yield return new WaitForSeconds(0.25f);
        DisplayPossibleActions(false);
        if (battleCanvas.UsingAbilities())
        {
            battleCanvas.resetCanvasSelections();
        }
    }

    private bool turnEnd = false;

    public void MakeMove(MoveHandler movement)
    {
        if (CurrentCharacter.CanMoveToPlatform(movement))
        {
            Player p = (Player)world.GetCurrentCharacter();
            p.getCurrentPlatform().SetIsActivePlatform(false);
            Vector3 dest = movement.gameObject.transform.position;
            p.SetPosition(new Vector3(dest.x, dest.y, 0));
            p.setCurrentPlatform(movement);
            DisplayPossibleActions(false);
            battleCanvas.OnTurnChange(false, false, false);
            StartCoroutine(CurrentCharacter.EndTurn());
        }
    }

    public void CharacterSleepTurn()
    {
        battleCanvas.OnTurnChange(false, false, false);
        StartCoroutine(CurrentCharacter.EndTurn());
    }



    public bool isDisplayingActions = false;
    // Display Movement & Attack
    public void DisplayPossibleActions(bool displayStatus)
    {
        isDisplayingActions = displayStatus;
        // Hide moveHandlers
        if (!displayStatus)
        {
            for (int x = 0; x < moveHandlers.Length; x++)
            {
                moveHandlers[x].DisplayMovement(displayStatus, this);
            }
        }
        else
        {
            // Show possible moves
            Debug.Log("CurrentCharacter: " + CurrentCharacter);
            List<MoveHandler> moveOptions = CurrentCharacter.getMovementPoints();
            moveOptions.ForEach((MoveHandler obj) => obj.DisplayMovement(displayStatus, this));
        }
    }
}
