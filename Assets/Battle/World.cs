﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class World : MonoBehaviour
{
    GameManagement gm;
    private int MAX_OBSTACLE_COUNT = 7;
    private BattleCanvas bcanvas;

    void Start()
    {
        gm = FindObjectOfType<GameManagement>();
        CreateBattleGround();
        //CreateWorld ();
        SetCharacters();
        bcanvas = FindObjectOfType<BattleCanvas>();
        StartCoroutine(StartRoundAfter());
    }


    IEnumerator StartRoundAfter()
    {
        yield return new WaitForSeconds(0.01f);
        ResetRound();
    }

    public float goDepth = 40;
    Vector3 v3ViewPort;
    Vector3 v3BottomLeft;
    Vector3 v3TopRight;
    public float distance = 2f;
    private float worldDistanceX, worldDistanceY;
    private Vector3 WorldSquareScale;

    void CreateBattleGround()
    {

        Vector3 LeftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        Vector3 RighBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        worldDistanceX = Vector3.Distance(new Vector3(LeftTop.x, 0, 0), new Vector3(RighBottom.x, 0, 0)) / 10;
        worldDistanceY = Vector3.Distance(new Vector3(0, LeftTop.y, 0), new Vector3(0, RighBottom.y, 0)) / 11;

        GameObject groundResource = (GameObject)Resources.Load("Ground");
        GameObject movementSquareResource = (GameObject)Resources.Load("MovePosition");
        GameObject battleMenuItem = (GameObject)Resources.Load("BattleMenuItem");
        GameObject battleButtons = (GameObject)Resources.Load("BattleButtons");

        WorldSquareScale = new Vector3(worldDistanceX, worldDistanceY, 0);

        groundResource.transform.localScale = WorldSquareScale;

        float yBound = groundResource.GetComponent<SpriteRenderer>().bounds.size.y;
        float xBound = groundResource.GetComponent<SpriteRenderer>().bounds.size.x;

        float startX = LeftTop.x + (xBound / 2);
        float startY = LeftTop.y - (yBound / 2);

        float boundsX, boundsY;
        Sprite sprite = Resources.Load<Sprite>("World/GrassGround/ground_rock");
        Sprite ground_1 = Resources.Load<Sprite>("World/GrassGround/backyard_02");
        Sprite ground_2 = Resources.Load<Sprite>("World/GrassGround/backyard_03");
        Sprite ground_3 = Resources.Load<Sprite>("World/GrassGround/backyard_53");
        Sprite ground_4 = Resources.Load<Sprite>("World/GrassGround/backyard_54");
        Vector3 groundPosition = new Vector3(startX, startY, 2f);
        int obstacle_count = 0;
        for (int k = 0; k < 11; k++)
        {
            float groundY = groundPosition.y;
            for (int i = 0; i < 10; i++)
            {
                GameObject ground;
                if (k != 10)
                {
                    ground = Instantiate(groundResource);
                }
                else
                {
                    ground = Instantiate(battleMenuItem);
                }
                ground.transform.position = groundPosition;
                boundsX = ground.GetComponent<SpriteRenderer>().bounds.size.x;
                boundsY = ground.GetComponent<SpriteRenderer>().bounds.size.y;

                groundPosition = new Vector3(ground.transform.position.x + boundsX, groundPosition.y, 2f); //groundPosition.y
                if (k != 10)
                {
                    MoveHandler hand = ground.AddComponent<MoveHandler>();
                    hand.GetComponent<MoveHandler>().setIDS(k, i);
                    ground.layer = LayerMask.NameToLayer("Movement");
                    if (k == 1 && i > 0 && i < 9)
                    {
                        ground.gameObject.tag = "StartTeam2";
                    }
                    else if (k == 8 && i > 0 && i < 9)
                    {
                        ground.gameObject.tag = "StartTeam1";
                    }
                    else
                    {
                        int range = Random.Range(0, 20);
                        if (range >= 17 && obstacle_count < MAX_OBSTACLE_COUNT)
                        {
                            ground.GetComponent<SpriteRenderer>().sprite = sprite;
                            ground.GetComponent<MoveHandler>().IsOccupied = true;
                            obstacle_count++;
                        }
                        else if (range == 2)
                        {
                            ground.GetComponent<SpriteRenderer>().sprite = ground_1;
                        }
                        else if (range == 5)
                        {
                            ground.GetComponent<SpriteRenderer>().sprite = ground_2;
                        }
                        else if (range == 3 || range == 7)
                        {
                            ground.GetComponent<SpriteRenderer>().sprite = ground_3;
                        }
                        else if (range == 10)
                        {
                            ground.GetComponent<SpriteRenderer>().sprite = ground_4;
                        }
                    }

                }
            }
            groundPosition = new Vector3(startX, groundY - (yBound), 2f);
        }
    }

    private List<Player> PlayerCharacters;
    private List<Ai> AiCharacters;

    void SetCharacters()
    {
        AiCharacters = new List<Ai>();
        PlayerCharacters = new List<Player>();
        GameObject Character = (GameObject)Resources.Load("MovableCharacter");
        GameObject[] StartPosition = GameObject.FindGameObjectsWithTag("StartTeam2");
        int pos = 0;
        foreach (Character c in gm.Opposite.GetCharacters())
        {
            Character copyCharacter = null;
            GameObject newAI = Instantiate(Character);
            newAI.transform.localScale = new Vector3(WorldSquareScale.x * 4.5f, WorldSquareScale.y * 3, WorldSquareScale.z);
            Ai aiComponent = newAI.AddComponent<Ai>();
            newAI.GetComponent<SpriteRenderer>().sprite = c.GetSprite();
            AiCharacters.Add(aiComponent);
            Vector3 start = StartPosition[pos].transform.position;
            copyCharacter = c;
            aiComponent.SetupCharacter(copyCharacter, StartPosition[pos].GetComponent<MoveHandler>());
            newAI.transform.position = new Vector3(start.x, start.y, -1);
            pos++;
        }


        StartPosition = GameObject.FindGameObjectsWithTag("StartTeam1");

        foreach (Character c in gm.PlayerTeam.GetCharacters())
        {
            GameObject player = Instantiate(Character);
            player.transform.localScale = new Vector3(WorldSquareScale.x * 4.5f, WorldSquareScale.y * 3, WorldSquareScale.z);//WorldSquareScale * 3; 
            Player playerComponent = player.AddComponent<Player>();
            player.GetComponent<SpriteRenderer>().sprite = c.GetSprite();

            PlayerCharacters.Add(playerComponent);
            Vector3 start = StartPosition[pos].transform.position;
            Character copyCharacter = c;
            playerComponent.SetupCharacter(copyCharacter, StartPosition[pos].GetComponent<MoveHandler>());
            player.transform.position = new Vector3(start.x, start.y, -1);
            pos++;
        }
    }

    void CreateWorld()
    {
        MoveHandler[] movePlatforms = GameObject.FindObjectsOfType<MoveHandler>();
        float currentRatio = (float)Screen.width / (float)Screen.height;

        for (int x = 0; x < movePlatforms.Length; x++)
        {

            SpriteRenderer sr = movePlatforms[x].gameObject.GetComponent<SpriteRenderer>();
            sr.transform.localScale = new Vector3(1, 1, 1);

            var width = sr.sprite.bounds.size.x;
            var height = sr.sprite.bounds.size.y;

            var worldScreenHeight = Camera.main.orthographicSize * 2.0;
            var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        }
    }

    bool TeamsAlive()
    {
        bool AiTeamAlive = false;
        bool PlayerTeamAlive = false;
        foreach (Ai a in AiCharacters)
        {
            if (a.isAlive())
            {
                AiTeamAlive = true;
                playerWon = false;
                break;
            }
        }
        foreach (Player p in PlayerCharacters)
        {
            if (p.isAlive())
            {
                PlayerTeamAlive = true;
                playerWon = true;
                break;
            }
        }
        return (AiTeamAlive && PlayerTeamAlive);
    }

    private void BattleEnd()
    {
        foreach (Ai a in AiCharacters)
        {
            a.GetCharacter().Reset();
        }
        foreach (Player a in PlayerCharacters)
        {
            a.GetCharacter().Reset();

        }
        gm.OnPlayerBattleEnd(playerWon);
        SceneManager.LoadScene(0);
    }

    private bool playerWon = false;

    public void EndTurn(bool isPlayer)
    {
        if (!TeamsAlive())
        {
            BattleEnd();
            return;
        }
        else if (isPlayer)
        {
            bool found = EndPlayerTurn();
            if (!found)
            {
                found = EndAiTurn();
                if (!found)
                {
                    ResetRound();
                }
            }
        }
        else
        {
            bool found = EndAiTurn();
            if (!found)
            {
                found = EndPlayerTurn();
                if (!found)
                {
                    ResetRound();
                }
            }
        }
    }

    public void ResetRound()
    {
        foreach (Ai c in AiCharacters)
        {
            c.SetMoved(false);
        }
        foreach (Player c in PlayerCharacters)
        {
            c.SetMoved(false);
        }
        EndTurn(false);
    }

    public bool EndPlayerTurn()
    {
        foreach (Ai c in AiCharacters)
        {
            if (!c.IsMoved() && c.isAlive())
            {
                bcanvas.SetUserButtonsState(false, true);
                current = c;
                c.Act();
                return true;
            }
        }
        return false;
    }

    public bool EndAiTurn()
    {
        foreach (Player c in PlayerCharacters)
        {
            if (!c.IsMoved() && c.isAlive())
            {
                current = c;
                c.BecomeCurrentCharacter();
                bcanvas.SetUserButtonsState(true, false);
                StartCoroutine(ShowMovementAfter(c));
                return true;
            }
        }
        return false;
    }

    private IEnumerator ShowMovementAfter(Player c)
    {
        yield return new WaitForSeconds(0.20f);
        FindObjectOfType<Controllers>().OnTurnChange(c);
        FindObjectOfType<Controllers>().DisplayPossibleActions(true);
    }

    private ICharacter current;

    public ICharacter GetCurrentCharacter()
    {
        return current;
    }

    private void ResetSelection()
    {

    }
}
