﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Player : Actor, ICharacter
{
    private Character myCharacter;
    private bool moved = false;
    public bool isActive = false;
    private BattleCanvas battleCanvas;

    protected override void OnStart()
    {
        battleCanvas = FindObjectOfType<BattleCanvas>();
    }

    public void SetupCharacter(Character c, MoveHandler currentPlatform)
    {
        this.myCharacter = c;
        this.currentPlatform = currentPlatform;
    }

    public Character GetCharacter()
    {
        return myCharacter;
    }

    public void MakeAttackHit(ICharacter character, Action<string> callback)
    {
        Debug.Log("Making Attack!");
        int attackdmg = myCharacter.getMeleeDamage();
        character.onAttacked(attackdmg, (isDeath) =>
        {
            callback(attackdmg.ToString());
            //callback (attackdmg.ToString ());
        });
        // if (!character.GetCharacter().Alive())
        //  {
        //SceneManager.LoadScene (0);
        // }
    }

    public void MakeAttackShoot(ICharacter character, Action<string> callback)
    {

        Debug.Log("Making Attack!");
        int attackdmg = UnityEngine.Random.Range(0, 4);
        character.onAttacked(attackdmg, (isDeath) =>
        {
            callback(attackdmg.ToString());
        });
    }

    protected override void OnUpdate()
    {

    }

    public bool isAlive()
    {
        return myCharacter.Alive();
    }

    public void onAttacked(int damage, Action<bool> callback)
    {
        StartCoroutine(AnimateDamage(damage));
        myCharacter.ReduceHealth(damage);
        if (myCharacter.Health <= 0)
        {
            Debug.Log("Death!");
            StartCoroutine(OnDeathAnimation(callback));
        }
        else
        {
            callback(false);
        }

        Debug.Log("character.GetHealth (): " + myCharacter.Health);
    }

    public void SetMoved(bool m)
    {
        this.moved = m;
        if (m)
        {
            isActive = false;
        }
    }

    public bool IsMoved()
    {
        return moved;
    }

    public void BecomeCurrentCharacter()
    {
        currentPlatform.SetIsActivePlatform(true);
        this.isActive = true;
        bool enableBow = myCharacter.getBow() != null;
        if (enableBow)
        {
            foreach (MoveHandler m in getMovementPoints())
            {
                if (m.IsOccupiedAI())
                {
                    enableBow = false;
                    break;
                }
            }
        }
        battleCanvas.OnTurnChange(enableBow, true, true);
    }

    public bool IsCurrentCharacter()
    {
        return isActive;
    }

    public void setCurrentPlatform(MoveHandler currentPlatform)
    {
        this.currentPlatform = currentPlatform;
    }

    public MoveHandler getCurrentPlatform()
    {
        return currentPlatform;
    }

    public Vector3 getPosition()
    {
        return this.gameObject.transform.position;
    }

    public void SetPosition(Vector3 position)
    {
        this.gameObject.transform.position = position;
    }

    public IEnumerator ShotCoroutine(Ai ai)
    {
        yield return new WaitForSeconds(0.25f);
        MakeAttackShoot(ai, (dmg) =>
        {
            battleCanvas.resetCanvasSelections();
            Debug.Log("Player made " + dmg + " dmg to AI");
            battleCanvas.OnTurnChange(false, false, false);
            StartCoroutine(EndTurn());
        });
    }

    public IEnumerator HitCoroutine(Ai ai)
    {
        yield return new WaitForSeconds(0.25f);
        MakeAttackHit(ai, (dmg) =>
        {
            Debug.Log("Player made " + dmg + " dmg to AI");
            battleCanvas.OnTurnChange(false, false, false);
            StartCoroutine(EndTurn());
        });
    }

    public IEnumerator EndTurn()
    {
        yield return new WaitForSeconds(0.05f);
        Debug.Log("Controller end turn");
        currentPlatform.SetIsActivePlatform(false);
        SetMoved(true);
        world.EndTurn(true);
    }
}