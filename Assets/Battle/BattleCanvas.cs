﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Linq;

public class BattleCanvas : MonoBehaviour
{
    GameObject attackButton, displayDataView, stats, selectedCharacter, titlebar,
        shotActive, shotDisable, sleepActive, sleepDisable;
    private Sprite[] turnImages, shootButtons;
    private Image firstTurnContainer, secondTurnContainer;

    void Start()
    {
        FindElements();
        InitializeButtons();
    }

    private bool isShooting = false;

    private Button shootButton, sleepButton;


    public bool UsingAbilities()
    {
        return isShooting;
    }

    public void resetCanvasSelections()
    {
        isShooting = false;
        ChangeShootButtonState(isShooting);
        Ai[] AIs = FindObjectsOfType<Ai>();
        foreach (Ai a in AIs)
        {
            a.onDisplayAttackBow(isShooting);
        }
    }

    void InitializeButtons()
    {
        shootButtons = new Sprite[2];
        shootButtons[0] = Resources.Load("Buttons/WoodButton", typeof(Sprite)) as Sprite;
        shootButtons[1] = Resources.Load("Buttons/WoodButtonActive", typeof(Sprite)) as Sprite;

        stats.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
            Debug.Log("Closing");
            displayDataView.SetActive(false);
        });

        displayDataView.transform.Find("CloseDisplayArea").GetComponent<Button>().onClick.AddListener(() =>
        {
            Debug.Log("Closing");
            displayDataView.SetActive(false);
        });

        shootButton = GameObject.Find("ShootButton").GetComponent<Button>();
        sleepButton = GameObject.Find("SleepButton").GetComponent<Button>();

        shootButton.onClick.AddListener(() =>
        {
            Debug.Log("Call shootButton");
            isShooting = !isShooting;
            ChangeShootButtonState(isShooting);
            Ai[] AIs = FindObjectsOfType<Ai>();
            foreach (Ai a in AIs)
            {
                a.onDisplayAttackBow(isShooting);
            }
        });
        shotActive = shootButton.transform.Find("Active").gameObject;
        shotDisable = shootButton.transform.Find("Disable").gameObject;

        sleepActive = sleepButton.transform.Find("Active").gameObject;
        sleepDisable = sleepButton.transform.Find("Disable").gameObject;

        sleepButton.onClick.AddListener(() =>
        {
            FindObjectOfType<Controllers>().CharacterSleepTurn();
        });

        OnTurnChange(false, false, false);
        displayDataView.SetActive(false);
    }

    private void ChangeShootButtonState(bool state)
    {
        Sprite sprite = state == false ? shootButtons[0] : shootButtons[1];
        Debug.Log("sprite: " + sprite + " state : " + state);
        shootButton.GetComponentInChildren<Image>().sprite = sprite;
    }

    private void FindElements()
    {
        firstTurnContainer = GameObject.Find("FirstTurnContainer").GetComponent<Image>();
        secondTurnContainer = GameObject.Find("SecondTurnContainer").GetComponent<Image>();
        turnImages = new Sprite[2];
        turnImages[0] = Resources.Load("Buttons/Button_round_no_turn", typeof(Sprite)) as Sprite;
        turnImages[1] = Resources.Load("Buttons/Button_round_turn", typeof(Sprite)) as Sprite;
        displayDataView = GameObject.Find("DisplayCharacter").gameObject;
        stats = displayDataView.transform.Find("Stats").gameObject;
        titlebar = stats.transform.Find("TitleBar").gameObject;
    }

    private bool ScanEnable = false;

    private void ChooseAttackTarget()
    {
        Ai[] Bots = FindObjectsOfType<Ai>();
        Vector3 ActivePlayerPosition = FindObjectOfType<Player>().gameObject.transform.position;
        bool found = false;
        for (int x = 0; x < Bots.Length; x++)
        {
            GameObject movementChild = Bots[x].gameObject;
            if (Vector2.Distance(new Vector2(ActivePlayerPosition.x, ActivePlayerPosition.y), new Vector2(movementChild.transform.position.x, movementChild.transform.position.y)) < 0.5f)
            {
                found = true;
                GameObject CharacterCanvas = movementChild.transform.Find("CharacterCanvas").gameObject;
                Image img = CharacterCanvas.GetComponentInChildren<Image>();
                if (ScanEnable)
                {
                    img.enabled = false;
                    SetAttack(img.gameObject, false, CharacterCanvas);
                }
                else
                {
                    img.enabled = true;
                    SetAttack(img.gameObject, true, CharacterCanvas);
                }
            }
        }

        if (found)
        {
            ScanEnable = !ScanEnable;
        }
    }

    public void OnTurnChange(bool shootActive, bool magicActive, bool skipTurnActive)
    {
        shootButton.enabled = shootActive;
        sleepButton.enabled = skipTurnActive;

        sleepActive.SetActive(skipTurnActive);
        sleepDisable.SetActive(!skipTurnActive);
        shotActive.SetActive(shootActive);
        shotDisable.SetActive(!shootActive);
    }

    private void SetAttack(GameObject img, bool attackEnable, GameObject characterCanvas)
    {
        Button AttackBtn = img.GetComponent<Button>();
        if (attackEnable)
        {
            AttackBtn.enabled = true;
            AttackBtn.onClick.AddListener(() =>
            {
                Debug.Log("Attack!");
                FindObjectOfType<Player>().MakeAttackHit(characterCanvas.GetComponentInParent<Ai>(), (dmg) =>
                {
                    StartCoroutine(OnDamageShown(characterCanvas, dmg));
                });

                AttackBtn.onClick.RemoveAllListeners();
            });
        }
        else
        {
            AttackBtn.enabled = false;
        }
    }

    IEnumerator OnDamageShown(GameObject characterCanvas, string dmg)
    {
        ScanEnable = true;
        ChooseAttackTarget();
        Text text = characterCanvas.GetComponentInChildren<Text>();
        text.text = dmg;
        text.enabled = true;
        yield return new WaitForSeconds(1f);
        text.enabled = false;

    }

    public void SetUserButtonsState(bool player, bool ai)
    {
        firstTurnContainer.sprite = player == true ? turnImages[1] : turnImages[0];
        secondTurnContainer.sprite = ai == true ? turnImages[1] : turnImages[0];
    }

    public void SelectedCharacter(Character character)
    {
        if (displayDataView.activeInHierarchy)
        {
            displayDataView.SetActive(false);
        }
        else
        {
            displayDataView.SetActive(true);
            string characterHealth = character.Health + " / " + character.MaxHealth,
            manaText = character.Mana + " / " + character.MaxMana;
            stats.transform.Find("CImage").GetComponent<Image>().sprite = character.GetSprite();
            stats.transform.Find("Health").GetComponent<Text>().text = characterHealth;
            stats.transform.Find("Clan").GetComponent<Text>().text = character.ClanName;
            stats.transform.Find("Strength").GetComponent<Text>().text = character.Strength.ToString();
            stats.transform.Find("Mana").GetComponent<Text>().text = characterHealth;
            titlebar.transform.Find("Name").GetComponent<Text>().text = character.GetName();

        }
    }
}
